#!/bin/env python3

import argparse
import pathlib
import sys

from PIL import Image, ImageDraw, ImageFont

FONT_FILE = pathlib.Path('fonts/CG-pixel-4x5/CG_pixel_4x5.ttf')
with FONT_FILE.open('rb') as f:
    FONT = ImageFont.truetype(f, size = 5)

def text2wall(text:str="Hello World", font=FONT):
    "Returns image representation of text on 53x7 pixel panel"
    wall = Image.new('1', (53,7), 0) # 1bit
    #wall = Image.new('L', (52,7), 255) # 8bit

    draw = ImageDraw.Draw(wall)

    draw.text((1,1), text ,font=font, fill=1)
    return wall

def wall_preview(wall:Image):
    "ASCII text preview of wall"
    wall_data = "".join(
        ["⬜" if commit==0 else "✅" for commit in list(wall.getdata())]
    )
    wall_ascii=""
    for wkdi in range(7):
        wall_ascii+=wall_data[wkdi*53:(wkdi+1)*53]+"\n"
    return wall_ascii

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Output text as 53(weeks)x7(days)  PNG image")
    parser.add_argument(
        "text", type=str,
        help="Text to place in image")
    parser.add_argument(
        "-o", "--output-file", type=pathlib.Path, default=None,
        help="output filename (defaults to stdout)")
    parser.add_argument(
        "--see-png", action="store_true",
        help="show wall as PNG image file (default)"
    )
    parser.add_argument(
        "--see-ascii", action="store_true",
        help="show wall as temp image file"
    )

    args = parser.parse_args()

    wall = text2wall(text=args.text)
    if args.output_file:
        with args.output_file.open('wb') as f: wall.save(f, "PNG")
    elif not args.see_png and not args.see_ascii:
        wall.save(sys.stdout, "PNG")
    if args.see_png:
        wall.show()
    if args.see_ascii:
        print(wall_preview(wall))

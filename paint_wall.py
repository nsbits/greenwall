#!/bin/env python3

from datetime import date, datetime, timedelta

from git import Repo
from PIL import Image

WEEKDAYS = ["Sunday", 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

def allsundays(year):
   d = date(year, 1, 1)                    # January 1st
   d += timedelta(days = 6 - d.weekday())  # First Sunday
   while d.year == year:
      yield d
      d += timedelta(days = 7)

def commit_message(dat:date=None):
    if not dat:
        dat = date.today()
    return (
        f"Just another day, just another time"
        f" {dat} {datetime.now().time().isoformat()}"
    )

def make_commit(repo:Repo, date_time:datetime=None, commit_message_func=commit_message):
    if not date_time:
        date_time = date_time.now()
    repo.git.commit(
        "--allow-empty",
        "--message", commit_message_func(date_time),
        "--date={}".format(date_time.isoformat()),
    )

def do_wall(wall:Image, year:date.year, repo:Repo, start:date=None):
    """
    Make commits based on wall.
    Where wall is a 53(weeks)x7(days) image, with 1-bit color.
    Color 0 for no commits, 1 for days with commits"""
    if start==None:
        startsun = list(allsundays(year))[0]
    else:
        startsun = start + timedelta(days = 6 - start.weekday())
    for week in range(1,53):
        for wkdi in range(1,7): #weekday index (Mon-Sat)
            to_commit = wall.getpixel((week, wkdi))
            if to_commit: 
                print(f"Doing work @ Week {week}, {WEEKDAYS[wkdi]}")
                make_commit(repo, date_time=startsun+timedelta(days=wkdi, weeks=week))

if __name__ == "__main__":
    import argparse
    import pathlib
    from text2wall import text2wall, wall_preview

    from git import InvalidGitRepositoryError

    parser = argparse.ArgumentParser(description="Create a bunch of commits on repo delivering a secret message")
    parser.add_argument("--year", type=int, default=datetime.now().year)
    parser.add_argument("--date", default=None, help="A start date from when to make commits. Use iso format (2022-03-04)")
    parser.add_argument("message", type=str, default="goo hara lives (^ y ^ *)")
    parser.add_argument("repodir", type=pathlib.Path, default=pathlib.Path("."))

    args = parser.parse_args()

    try:
        repo = Repo(args.repodir)
        git = repo.git
    except InvalidGitRepositoryError as E:
        print(E.args)
        E.add_note("Make sure repodir is valid git repo.")
        raise E

    wall = text2wall(args.message)
    do_wall(wall, year=args.year, start=date.fromisoformat(args.date), repo=repo)
    print("Expected output:\n{}".format(wall_preview(wall)))

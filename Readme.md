
# Greenwall

Greenify your git_hub code wall

## Introductionn

Github wall is cute, but it's very hackable. Here we demonstrate how easy it is by painting our 'contribution' wall with a message.  

The program makes commits on a repo of your choice that will show up in Github's contribution graph associated with your account.  
Provided you have associated the repos e-mail with one recognized by Github that is.

For gotchas see [Why are my contributions not showing up on my profile](https://docs.github.com/en/account-and-profile/setting-up-and-managing-your-github-profile/managing-contribution-settings-on-your-profile/why-are-my-contributions-not-showing-up-on-my-profile).  

## Disclaimer
Note: This program has no undo(s).

YMMV and you are solely responsible for your own graffiti ( \^ \^)

And also it's cleanup, should you need it :)


## Usage
    # Install dependencies
    poetry install

    # Paint your wall
    poetry run ./paint_wall.py --year 2022 "Short msg :)" /tmp/gitrepodir

## Self Promotion
See [self-promotion.md](self-promotion.md)

## Related / Inspiration

https://github.com/angusshire/greenhat
